

# Copyright 2019 Google LLC. This software is provided as-is, without warranty or representation
# for any use or purpose. Your use of it is subject to your agreement with Google.

from googleapiclient import discovery
from oauth2client.client import GoogleCredentials
from google.cloud import logging
import http.client
import time
import os


#-------------------------------------------------------------------------------------------------------------
#  Configurable Variables
#-------------------------------------------------------------------------------------------------------------

# any and all vars can be configured as environment vars
# in the cloud function.  Use the following as an example
# panIPaddress = os.environ.get('panIPaddress') 
# or
# panVpc = {os.environ.get('outsidevpc1'):[os.environ.get('outside_vpc_name')]}

# Palo Alto configuration
panIPaddress = "www.python.org"
panPath = "/kjh"

# GCP Configuration dict support anynumber of interfaces on equal number of vpc
panVpc = {

"outside":
["vpc-flowlogs",
"us-central1-a",
"someroute",
"10.10.10.10/32",
"default",
"instance-1",
1000],

"inside":
["vpc-flowlogs",
"us-central1-a",
"someroute2",
"99.10.10.10/32",
"default2",
"instance-2",
1000]
}

#-------------------------------------------------------------------------------------------------------------
#  End Configurable Variables
#-------------------------------------------------------------------------------------------------------------


# will use creds from Cloud Function   DO NOT CHANGE
credentials = GoogleCredentials.get_application_default()
service = discovery.build('compute', 'v1', credentials=credentials)

#-------------------------------------------------------------------------------------------------------------
#  Function write entry.  Write to stack driver.  Requires log name, log message, and severity
#-------------------------------------------------------------------------------------------------------------
def write_entry(logger_name, message, severity):
    """Writes log entries to the given logger."""
    logging_client = logging.Client()

    # This log can be found in the Cloud Logging console under 'Custom Logs'.
    logger = logging_client.logger(logger_name)


    # Simple text log with severity.
    logger.log_text(message, severity=severity)

#-------------------------------------------------------------------------------------------------------------
#  Function testPANapi.  Test connection to PAN API.  
#-------------------------------------------------------------------------------------------------------------
def testPANapi():

    conn = http.client.HTTPSConnection(panIPaddress)

    try:
        conn.request("GET", panPath)
        r1 = conn.getresponse()
        if r1.status == 200:
            return True

    except:
        m = "firewall can not be reached.  Failover script is malfunctioning"
        write_entry("check-pan", m, "ERROR")
        quit()


#-------------------------------------------------------------------------------------------------------------
#  Function checkroute.  Check if backup route is already installed
#-------------------------------------------------------------------------------------------------------------
def checkroute():
    # check if the route is already installed
    # try the request.  If the route name is installed response returns http 200 
    # If not installed resonse fails

    fail = False

    for i, value in panVpc.items():
        request = service.routes().get(project=value[0], route=value[2])
        try:
            response = request.execute()
        except:
            fail = True
    if fail:

        return True

    else:
        return False

#-------------------------------------------------------------------------------------------------------------
#  Function changeRoute.  Add a new route with higher priority
#-------------------------------------------------------------------------------------------------------------
def changeRoute():

    for i, value in panVpc.items():

        route_body = {

            "destRange": value[3],
            "name": value[2],
            "network": "projects/{}/global/networks/{}".format(value[0], value[4]),
            "nextHopInstance": "projects/{}/zones/{}/instances/{}".format(value[0], value[1], value[5]),
            "priority": value[6],
            "tags": []

                }
        request = service.routes().insert(project=value[0], body=route_body)
        try:

            response = request.execute()
            logString = "Backup route {} for {} installed".format(route_body.get("name"), route_body.get("destRange"))
            write_entry("check-pan", logString, "ERROR")
        except:

            logSting = "The route could not be installed"
            write_entry("check-pan", logString, "ERROR")
#-------------------------------------------------------------------------------------------------------------
#  Function checkpan.  Loop to check FW every five seconds
#-------------------------------------------------------------------------------------------------------------
def checkPan():
    trigger = 0 # a counter to trigger changeRoute()

    #number of failed attempts before new route is installed
    failTime = 3

    #while loop runa doe 59 seconds
    time_end = time.time() + 119
    while time.time() < time_end:
        if testPANapi():
            write_entry("check-pan", "Firewall responsed", "INFO")
            time.sleep(5)

        else:
            write_entry("check-pan", "Firewall did not responsed", "ERROR")
            time.sleep(5)
            trigger = trigger + 1

        if trigger == failTime:
            #run gcp command
            changeRoute()
            quit()
#-------------------------------------------------------------------------------------------------------------
#  Function cf_kickoff.  Function to call from the cloud function
#-------------------------------------------------------------------------------------------------------------
def cf_kickoff(request):

    if checkroute():
        checkPan()
    else:
        write_entry("check-pan", "route is already installed", "ERROR")
        quit()
